function setupSkeletonTemplate() {
    let wrap = document.getElementById("wrap");
    wrap.innerHTML = `<div class="title">
                    <div class="skeleton skeleton-text"></div>
                </div>
                <div class="contents">
                    <a class="undefined">
                        <div class="card">
                            <img class="skeleton">
                            <div class="content-spacing">
                                <div class="skeleton skeleton-text"></div>
                                <div class="skeleton skeleton-text"></div>
                                <div class="skeleton skeleton-text last"></div>
                                <button>Add to cart</button>
                            </div>
                        </div>
                    </a>
                </div>`

    let grid = document.querySelector('.contents')
    let cardTemplate = document.getElementsByClassName('undefined')[0]

    let count = 0;
    while(count < 5){
        grid.append(cardTemplate.cloneNode(true))
        count ++
    }
}

function fetchData(API) {
    return fetch(API)
        .then((data) => {
            if (data.ok) {
                return data.json();
            } else {
                throw `${data.statusText} ${data.status}`;
            }
        });
}

function groupDataBasedOnCategory(items) {
    return items.reduce((acc, item) => {
        if (acc[item.category]) {
            acc[item.category].push(item);
        } else {
            acc[item.category] = [item];
        }
        return acc;
    }, {});
}

function createAElement(tag, ClassName, idName, content) {
    let div = document.createElement(tag);
    div.classList.add(ClassName);
    if (content) {
        div.innerHTML = content;
    }
    if (idName) {
        div.setAttribute("id", idName)
    }
    return div;
}

function getAllProducts(API) {
    fetchData(API)
        .then((items) => {
            if (items.length === 0) {
                throw "No products available";
            } else {
                return groupDataBasedOnCategory(items);

            }
        })
        .then((groupedObjects) => {
            let section = document.getElementsByClassName("section-one-part-two");
            section[0].innerHTML = ""
            Object.entries(groupedObjects).forEach(([key,value])=>{
                let titleContent = `<div class="title">
                                        <h1>${key}</h1>
                                    </div>`
                let wrapDiv = createAElement("div", "wrap", "wrap", titleContent);
                let content = createAElement("div", "contents")
                value.forEach((item) => {
                    let cardContent = `
                        <img src=${item.image} alt="" srcset="">
                        <div class="content-spacing">
                            <h3>${item.title}</h3>
                            <h4>$${item.price}</h4>
                            <p> ${item.description}</p>
                            <h4> Rating - ${item.rating["rate"]} | Reviews - ${item.rating["count"]} </h4>
                            <button> View Product </button>
                        </div>
                    `
                    let card = createAElement("div", "card", null, cardContent);
                    let aTag = createAElement("a")
                    aTag.setAttribute("href", `https://ecommerce-products-page-using-an-api.vercel.app/productPage.html?productId=${item.id}`)
                    aTag.appendChild(card)
                    content.appendChild(aTag)
                })
                wrapDiv.appendChild(content)
                section[0].appendChild(wrapDiv)
            })
            


        })
        .catch((error) => {
            let section = document.getElementsByClassName("section-one-part-two");
            let errorContent = "";
            if (typeof error === "string") {
                errorContent = `<div class="title">
                                        <h1>${error}</h1>
                                    </div>`
            }
            errorContent = `<div class="title">
                                    <h1>Error while fetching the products.</h1>
                                </div>`

            let wrapDiv = createAElement("div", "wrap", null, errorContent);
            section[0].appendChild(wrapDiv)

        })


}

function getLimit(element) {
    setupSkeletonTemplate();
    if (event.key === 'Enter') {
        getAllProducts(`https://fakestoreapi.com/products?limit=${element.value}`)

    }

}

// Select by category.

function createDropDown() {
    fetchData('https://fakestoreapi.com/products/categories')
        .then((data) => {
            let div = document.getElementById("dropdown"),
                frag = document.createDocumentFragment(),
                select = document.createElement("select");
            select.setAttribute("onChange", "selectedCategory()")
            select.classList.add("dropdown")
            data.forEach((category, index) => {
                if (index === 0) {
                    select.options.add(new Option("All", true));
                }
                select.options.add(new Option(category));
            });


            frag.appendChild(select);
            div.appendChild(frag);


        })

}

createDropDown()

function selectedCategory(selectedValue) {
    if (!selectedValue) {
        selectedValue = document.getElementsByClassName("dropdown")[0].value;
    }
    document.getElementsByClassName("dropdown")[0].value = selectedValue;
    setupSkeletonTemplate();
    if (selectedValue === "true") {
        getAllProducts("https://fakestoreapi.com/products");
    } else {
        getAllProducts(`https://fakestoreapi.com/products/category/${selectedValue}`)
    }


}

// Sort
function sort(element) {
    setupSkeletonTemplate();
    document.getElementsByClassName("dropdown")[0].value = true;
    if (!element.name || element.name === "asc") {
        document.getElementsByClassName("sort-icon")[0].classList = "fa-solid fa-arrow-up-wide-short fa-3x sort-icon"
        document.getElementsByClassName("sort-icon")[0].name = "desc"
        getAllProducts('https://fakestoreapi.com/products?sort=desc')

    } else {
        document.getElementsByClassName("sort-icon")[0].classList = "fa-solid fa-arrow-down-short-wide fa-3x sort-icon"
        document.getElementsByClassName("sort-icon")[0].name = "asc"
        getAllProducts('https://fakestoreapi.com/products?sort=asc')
        

    }
}


function getCategories() {
    let section = document.getElementsByClassName("section-one-part-two");
    section[0].innerHTML = ""
    fetchData('https://fakestoreapi.com/products/categories')
        .then((data) => {
            data.forEach((category) => {
                let titleContent = `<div class="title">
                                        <h1><a onclick="setCategory(this)">${category}</a></h1>
                                    </div>`
                let wrapDiv = createAElement("div", "wrap", "wrap", titleContent);


                section[0].appendChild(wrapDiv)
            })
        })


}
function setCategory(element) {
    selectedCategory(element.textContent);
}



setupSkeletonTemplate();

const GET_ALL_PRODUCTS_API = "https://fakestoreapi.com/products";

getAllProducts(GET_ALL_PRODUCTS_API);



const mediaQuery = window.matchMedia('(max-width: 420px)')
// Check if the media query is true
if (mediaQuery.matches) {
    let icon = document.getElementsByClassName("sort-icon")[0]
    icon.classList = ""
    icon.setAttribute("class","fa-solid fa-arrow-down-short-wide fa-3x sort-icon")
    // console.log();
}