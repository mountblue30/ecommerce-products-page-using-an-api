function fetchData(API) {
    return fetch(API)
        .then((data) => {
            if (data.ok) {
                return data.json();
            } else {
                throw `${data.statusText} ${data.status}`;
            }
        });
}

function groupDataBasedOnCategory(items) {
    return items.reduce((acc, item) => {
        if (acc[item.category]) {
            acc[item.category].push(item);
        } else {
            acc[item.category] = [item];
        }
        return acc;
    }, {});
}

function createAElement(tag, ClassName, idName, content) {
    let div = document.createElement(tag);
    div.classList.add(ClassName);
    if (content) {
        div.innerHTML = content;
    }
    if (idName) {
        div.setAttribute("id", idName)
    }
    return div;
}

function getSingleProduct(API) {
    fetchData(API)
        .then((item) => {
            let sectionOne = document.getElementsByClassName("section-one-part-one");
            let sectionTwo = document.getElementsByClassName("section-one-part-two");
            sectionOne[0].innerHTML = `<img src=${item.image} alt="" srcset="">`
            let cardContent = `
                <div class="card">
                    <h3>${item.title}</h3>
                    <h4>$${item.price}</h4>
                    <p> ${item.description}</p>
                    <h4> Rating - ${item.rating["rate"]} | Available pieces - ${item.rating["count"]} </h4>
                </div>
            `
            
            sectionTwo[0].innerHTML = cardContent;
            
        })
        
        .catch((error) => {
            let section = document.getElementsByClassName("section-one-part-two");
            let errorContent = "";
            if (typeof error === "string") {
                errorContent = `<div class="title">
                                        <h1>${error}</h1>
                                    </div>`
            } else {
                errorContent = `<div class="title">
                                        <h1>Error while fetching the API.</h1>
                                    </div>`
            }
            let wrapDiv = createAElement("div", "wrap", null, errorContent);
            section[0].appendChild(wrapDiv)
        })


}


window.onload = function () {
    let url = document.location.href,
        params = url.split('?')[1].split('&')[0].split("=")[1]
    getSingleProduct(`https://fakestoreapi.com/products/${params}`)    
}


