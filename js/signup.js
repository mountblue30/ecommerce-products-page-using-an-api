function containsSpecialChars(str) {
    const specialChars =
        '[`!@#$%^&*()_+-=[]{};\':"\\|,.<>/?~]/';
    return specialChars
        .split('')
        .some((specialChar) => str.includes(specialChar));
}

function passwordFormatChecker(password) {
    let validRegex = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    return validRegex.test(password)
}

function passwordValidator(passwordOne, passwordTwo){
    return passwordOne === passwordTwo;
}
function checkValidName(value){
    return !/^[a-zA-Z]+$/.test(value)
}
function checkValidEmail(value){
    return !/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(value);
}
function inputValidation(event) {
    event.preventDefault();
    let firstName = document.getElementById("FirstName");
    let lastName = document.getElementById("LastName");
    let email = document.getElementById("email");
    let password = document.getElementById("password");
    let cnfrmPassword = document.getElementById("cnfrm-password");
    let checkBox = document.getElementById("terms");
    let signUp = true;
    if (checkValidName(firstName.value)) {
        let label = document.getElementById(firstName.name)
        label.innerHTML = "This field should only contain alphabets."
        firstName.style.border = "2px solid red"
        signUp = false;
    } else {
        let label = document.getElementById(firstName.name)
        label.innerHTML = ""
        firstName.style.border = "2px solid green"
    }
    if (checkValidName(lastName.value)) {
        let label = document.getElementById(lastName.name)
        label.innerHTML = "This field should only contain alphabets."
        lastName.style.border = "2px solid red"
        signUp = false;

    } else {
        let label = document.getElementById(lastName.name)
        label.innerHTML = ""
        lastName.style.border = "2px solid green"


    }
    if(checkValidEmail(email.value)){
        let label = document.getElementById(email.name)
        label.innerHTML = "Doesn't follow email conventions."
        email.style.border = "2px solid red"
        signUp = false;

    } else {
        let label = document.getElementById(email.name)
        label.innerHTML = ""
        email.style.border = "2px solid green"


    }
    if (!passwordFormatChecker(password.value)){
        let label = document.getElementById(password.name);
        label.innerHTML = "Must be minimum 8 chars with a uppercase,lowercase, symbol and numbers."
        password.style.border = "2px solid red"
        signUp = false;

    } else {
        let label = document.getElementById(password.name);
        label.innerHTML = ""
        password.style.border = "2px solid green"

    }
    if (cnfrmPassword.value === ""){
        let label = document.getElementById(cnfrmPassword.name);
        label.innerHTML = "This field should not be empty."
        cnfrmPassword.style.border = "2px solid red"
        signUp = false;
        
    } 
   
    if(cnfrmPassword.value.trim() === ""){
        let label = document.getElementById("cnfrm-pass");
        label.innerHTML = "This field should not be empty."
        cnfrmPassword.style.border = "2px solid red"
        signUp = false;
    } else {
        if (passwordValidator(password.value, cnfrmPassword.value)) {
            let label = document.getElementById(cnfrmPassword.name);
            label.innerHTML = ""
            cnfrmPassword.style.border = "2px solid green"

        } else {

            let label = document.getElementById(cnfrmPassword.name);
            label.innerHTML = "Passwords doesn't match."
            cnfrmPassword.style.border = "2px solid red"
            signUp = false;

        }
        
    }

    
    if(checkBox.checked){
        let label = document.getElementById("terms-cons");
        label.innerHTML = ""
        

    } else {
        let label = document.getElementById("terms-cons");
        label.innerHTML = "Accept terms and conditions."
        signUp = false;

    }
    if(signUp){

        let user = {};
        user[email.value] = {};
        user[email.value]["firstname"] = firstName.value
        user[email.value]["lastname"] = lastName.value
        user[email.value]["password"] = password.value
        localStorage.setItem("user",JSON.stringify(user));
        let card = document.getElementsByClassName("card");
        card[0].innerHTML = "";
        let element = document.createElement("p");
        element.innerHTML = "Signup successful. Click the HomePage button to return to homepage."
        card[0].appendChild(element)
        let button = document.createElement("a");
        button.setAttribute("onclick","redirectToHome()");
        button.innerHTML = "HomePage";
        card[0].appendChild(button)
        let logout = document.getElementsByClassName("logout")[0]
        logout.style.display = "block"
        
    }
    
    
}

function redirectToHome(){
    window.location = "/"
}
function logout(){
    localStorage.clear();
    let card = document.getElementsByClassName("card");
    card[0].innerHTML = "";
    let element = document.createElement("p");
    element.innerHTML = "Log out successful."
    card[0].appendChild(element)
    window.location.reload();
}

function loggedIn(){
    let card = document.getElementsByClassName("card");
    card[0].innerHTML = "";
    let element = document.createElement("p");
    element.innerHTML = "You are currently logged in. Click logout to perform other operations."
    card[0].appendChild(element)

}

if (localStorage.getItem("user")) {
    loggedIn()
} else {
    let logout = document.getElementsByClassName("logout")[0]
    logout.style.display = "none"

}



const openModalButtons = document.querySelectorAll('[data-modal-target]')
const closeModalButtons = document.querySelectorAll('[data-close-button]')

const overlay = document.getElementById("overlay")

openModalButtons.forEach(button => {
    button.addEventListener('click',()=>{
        const modal = document.querySelector(button.dataset.modalTarget)
        openModal(modal);
    })
})

overlay.addEventListener("click",()=>{
    const modals = document.querySelectorAll(".modal.active")
    modals.forEach(modal => {
        closeModal(modal)
    })
})
closeModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = button.closest(".modal")
        closeModal(modal);
    })
})

function openModal(modal){
    if (modal == null) {
        return
    } else {
        modal.classList.add("active")
        overlay.classList.add("active")

    }
}

function closeModal(modal) {
    if (modal == null) {
        return
    } else {
        modal.classList.remove("active")
        overlay.classList.remove("active")

    }
}